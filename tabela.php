<?php
session_start();

include("classe/conexao.php");
include("classe/verificarsession.php");
include("classe/real_menu.php");

	$sql_code = "SELECT t1.ID_Usuario, t1.Nome, t1.Email, t1.CPF, t2.DDD_Telefone, t2.Telefone, t3.Nome_Perfil from Usuario as t1\n"

    . "inner join Telefone as t2 on t1.ID_Usuario = t2.ID_Usuario\n"

    . "inner join Perfil as t3 on t1.ID_Perfil = t3.ID_Perfil"

    . "    order by t1.Nome asc";
	$sql_query = $mysqli->query($sql_code) or die($mysqli->error);
	$linha = $sql_query->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title> Vestibular FAC DF </title>
	<link rel="stylesheet" href="CSS/tabela.css">
	<link rel="shortcut icon" href="Imagens/facdf-cutout.png">

</head>

<body bgcolor="#1D5991"> </body>

<center>
  <br><br><br>
  <center>
	<br><img src="Imagens/controle-cutout.png" width="140" />
  </center>
  <br><br>
	<a style="color: white;" href="cadastro.php?p=cadastrar"><button style='padding: 5px; width: 300px'>Cadastrar um usuario</button></a> <br>

  <br>
  <table style="height:200px" border=6 cellpadding=15 color=red> </br>

    
	<td><center><b>Nome</td>
    <td><center><b>E-mail</td>
    <td><center><b>CPF</td>
    <td><center><b>DDD</td>
    <td><center><b>Telefone</td>
    <td><center><b>Perfil</td>
    <td></td>
 
    <?php
    do {
    ?>
      <tr>
        <td><?php echo $linha['Nome']; ?></td>
        <td><?php echo $linha['Email']; ?></td>
        <td><?php echo $linha['CPF']; ?></td>
        <td><center><?php echo $linha['DDD_Telefone']; ?></td>
        <td><?php echo $linha['Telefone']; ?></td>
        <td><?php echo $linha['Nome_Perfil']; ?></td>
        <td>
          <a href="editar.php?p=editar&usuario=<?php echo $linha['ID_usuario']; ?>">Editar</a> |
          <a href="javascript: if(confirm('Tem certeza que deseja deletar o usuário <?php echo $linha['Nome']; ?>?'))
			location.href='deletar.php?p=deletar&usuario=<?php echo $linha['ID_Usuario']; ?>';">Deletar</a>
        </td>
      </tr>
    <?php } while ($linha = $sql_query->fetch_assoc()); ?>
</center>
</table>
</html>