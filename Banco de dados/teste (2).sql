-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 14-Jul-2020 às 00:36
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `teste`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `questoes`
--

CREATE TABLE `questoes` (
  `id_questao` int(11) NOT NULL,
  `questao` varchar(250) NOT NULL,
  `tipo_questao` varchar(50) NOT NULL,
  `alternativa_a` varchar(150) NOT NULL,
  `alternativa_b` varchar(150) NOT NULL,
  `alternativa_c` varchar(150) NOT NULL,
  `alternativa_d` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `questoes`
--

INSERT INTO `questoes` (`id_questao`, `questao`, `tipo_questao`, `alternativa_a`, `alternativa_b`, `alternativa_c`, `alternativa_d`) VALUES
(20, 'Qual desses times não tem mundial?', 'POR', 'Palmeiras', 'Palmeiras', 'Palmeiras', 'Palmeiras'),
(21, '1+1', 'RC', '1', '3', '5', '2'),
(22, 'Qual a cor do céu?', 'POR', 'Marrom', 'Verde', 'Azul', 'Vermelho');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(20) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `email` varchar(500) NOT NULL,
  `curso` varchar(100) NOT NULL,
  `cpf` varchar(14) DEFAULT NULL,
  `telefone` int(20) NOT NULL,
  `senha` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `email`, `curso`, `cpf`, `telefone`, `senha`) VALUES
(32, 'fernandinho', 'fernandinho@gmail.com', 'RH', '849.108.040-13', 12121212, '123'),
(39, 'teste', 'teste@gmail.com', 'ADS', '414.141.414-14', 12121212, '1234'),
(44, 'as', 'as@as', 'ADS', '414.141.434-31', 1212, '1234'),
(45, 'AQAQ', '1212@AA', 'ADS', '122.222.222-22', 1212, '1212');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `questoes`
--
ALTER TABLE `questoes`
  ADD PRIMARY KEY (`id_questao`);

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `questoes`
--
ALTER TABLE `questoes`
  MODIFY `id_questao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
