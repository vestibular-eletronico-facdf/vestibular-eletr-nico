<?php

session_start();

include("classe/verificarsession.php");
include("classe/conexao.php");
include("classe/real_menu.php");

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Cadastro FACDF</title>
  <link rel="stylesheet" href="CSS/cadastro.css">
  <link rel="shortcut icon" href="Imagens/facdf-cutout.png">


  <head>
    <script type="text/javascript">
      function formatar_mascara(src, mascara) {
        var campo = src.value.length;
        var saida = mascara.substring(0, 1);
        var texto = mascara.substring(campo);
        if (texto.substring(0, 1) != saida) {
          src.value += texto.substring(0, 1);
        }
      }
    </script>
  </head>

<body bgcolor="#1d5991">
  <form action="#" method="post">
  </form>

</head>


  <br><br><br>
  <center>
    <img src="Imagens/lapis.png" width="150" />
  </center>
  <div class='form'>
    <form method="POST" action="classe/processo_cad.php">
	
		<center>
        <input type="text" class="inputLogin" name="nome" placeholder="Nome completo:" required autofocus> <br>
        <br><input type="text" class="inputLogin" name="email" placeholder="E-mail" required autofocus><br>
        <br><input type="number" class="inputLogin" name="DDD" placeholder="DDD" required autofocus>
        <input type="number" class="inputLogin" name="telefone" placeholder="Telefone" required autofocus<br><br>


        <br><select name="cursos">
          <br>
          <option value="">Escolha o Curso</option>
          <option value="3"> Gestão em Tecnologia da informaçao</option>
          <option value="4"> Analise e Desenvolvimento de sistemas</option>
          <option value="6"> Pedagogia </option>
          <option value="1"> Gestão em recursos humanos </option>
          <option value="2"> Gestão comercial </option>
          <option value="5"> Administração </option>
        </select> <br>

        <label></label>
        <br> <input name="cpf" placeholder="CPF" method="post" type="text" maxlength="14" onkeypress="formatar_mascara(this,'###.###.###-##')"> <br>

        <label></label>
        <br> <input name="senha" placeholder="senha" method="post" type="password" <br><br>

        <br> <input type="submit" name="cadastro" value="Gravar" class='btn'>
      </center>

    </form>
  </div>
</body>

</html>